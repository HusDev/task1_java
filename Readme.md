- The src/main/Main.java file contains 3 methods:
    - main(int Max)          :int    (take the Maximum number and return the counter)
    - check89Or1(int num)    :List   (take any number and return the Chain number that end up with 1 or 89)
    - getTheLastNumber(List) :int    (take the chain of the number and reuturn last one 1 or 89)

- The src/test/TestNumbers.java file contains 3 test cases