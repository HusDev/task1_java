import java.util.ArrayList;
import java.util.List;

public class Main {

    public static List check89or1(int num){
        int sum = 0  ;
        List<Integer> list = new ArrayList<Integer>(); //O(1)
        do{   // O(n^2)
//number to digit and add their square
            while(num != 0){
                int temp = num%10 ;
                num = num/10 ;
                sum += Math.pow(temp,2)   ;
            }
            num = sum  ;
            list.add(sum);
            sum = 0 ;
        }while(num != 89 && num !=1) ;
        return list ;
    }

    public static int getTheLastNumber(List list){
        int lastNum = (int) list.get(list.size() - 1); //O(1)
        return lastNum  ;
    }





   public static int chain(int a){
//       vars
        int counter = 0 ;
        int num = 0 ;
        int MAX = a;
//Count from 0 to MAX (for Example 10 million)
        for (int i=1 ; i<MAX;  i++){
            num = i ;

           if (getTheLastNumber(check89or1(num)) == 89){
               counter++;
           }

        }
        return counter ;
    }


    public static void main(String[] args){
       System.out.println(chain(1000000));
    }



}





/*
---------------------------------------------------------
                                                             COST         TIME
                                                             ____         ____
        int counter = 0 ;                                     C1            -
        int num = 0 ;                                         C2            -
        int MAX = a;                                          C3            -
        for (int i=1 ; i<MAX;  i++){                          C4            O(n)
            num = i ;                                         C5            -
           if (getTheLastNumber(check89or1(num)) == 89){      C6            O(n^2)
               counter++;
           }
        }
        return counter ;

The total time will be: O(n^3)

 */