import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;


public class TestNumbers {
    @Test
    public void testNumbersFrom1ToX(){
        Main m = new Main() ;
//    MAX number
        int MAX = 5 ;
//    Your expectation
    int expected = 3 ;
        assertEquals(expected,m.chain( MAX) );
    }




    @Test
    public void check89Or1(){
    Main m = new Main() ;
    //    your input
    int input = 145 ;

    //    your expected output
    List<Integer> expected =  Arrays.asList(42, 20, 4, 16, 37, 58, 89); ;
    assertEquals(expected, m.check89or1(input));
}



@Test
    public void getTheLastNumber(){
        Main m = new Main() ;
//        input
    List<Integer> list = Arrays.asList(42, 20, 4, 16, 37, 58, 89);
    //    your expected output
    int expected = 89 ;
    assertEquals(expected, m.getTheLastNumber(list));

}


}
